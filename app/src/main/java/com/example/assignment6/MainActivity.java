package com.example.assignment6;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    ListView listView;
    List<Map<String,String>> studentList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.lv_student);

        new GetStudents().execute();
    }

    private class GetStudents extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MainActivity.this,"Json Data is downloadig",Toast.LENGTH_LONG).show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String url = "https://run.mocky.io/v3/ade12cda-3edb-44bb-b3e1-129166060de2";
            String jsonStr = sh.makeServiceCall(url);

            // Log.e(TAG, "Response from url: " + jsonStr);
//            System.out.println(jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray students = jsonObj.getJSONArray("Students");

                    for(int i=0; i<students.length(); i++) {
                        JSONObject student = students.getJSONObject(i);

                        try {
                            HashMap<String,String> result = new ObjectMapper().readValue(student.toString(), HashMap.class);
                            studentList.add(result);
                            System.out.println("Result: " + result);
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (final JSONException e) {
                    // Log.e(TAG, "Json parsing error: " + e.getMessage());
                    System.out.println(e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                }

            } else {
                //Log.e(TAG, "Couldn't get json from server.");
                System.out.println("Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            CustomAdapter adapter = new CustomAdapter(MainActivity.this, studentList);

            listView.setAdapter(adapter);
        }
    }

    public class CustomAdapter extends ArrayAdapter<Map<String, String>> {
        Context context;
        List<Map<String, String>> students;

        CustomAdapter (Context c, List<Map<String, String>> students) {
            super(c, R.layout.student_row, students);
            this.context = c;
            this.students = students;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.student_row, parent, false);

            TextView firstName = row.findViewById(R.id.tv_firstname);
            TextView lastName = row.findViewById(R.id.tv_lastname);
            TextView age = row.findViewById(R.id.tv_age);
            TextView enrollmentNumber = row.findViewById(R.id.tv_enrollmentNumber);
            TextView degree = row.findViewById(R.id.tv_class);

            firstName.setText("First Name: " + students.get(position).get("First name"));
            lastName.setText("Lasr Name: " + students.get(position).get("Last name"));
            age.setText("Age: " + students.get(position).get("age"));
            enrollmentNumber.setText("Enrollment Number: " + students.get(position).get("enrollment number"));
            degree.setText("Class: " + students.get(position).get("Class"));

            return row;
        }
    }
}
